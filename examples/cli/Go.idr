module Go

import Data.Vect
import Server.EDSL.Lens
import Server
import Server.CLI
import Optics.Dependent
import Data.Sum
import Data.Product

%hide Prelude.(/)

data Stone = White | Black | Empty

HasParser Stone where
  partialParse = string "w" *> pure White
             <|> string "b" *> pure Black
             <|> string "+" *> pure Empty

Show Stone where
  show White = "w"
  show Black = "b"
  show Empty = "+"

Board : Type
Board = Vect 9 (Vect 9 Stone)

export
{n : Nat} -> HasParser (Fin n) where
  partialParse = do nat <- partialParse {t=Nat}
                    let Just v = natToFin nat n
                      | Nothing => fail "number too big"
                    pure v
export
{n : Nat} -> Display (Fin n) where
  display = "Fin \{show n}"

export
{n : Nat} -> Default a => Default (Vect n a) where
  def = replicate n def

emptyBoard : Board
emptyBoard = replicate 9 (replicate 9 Empty)

updateVect : Para (MkB (Fin n) Unit)
                  (Dup (Vect n a))
                  (MkB a a)
updateVect = MkContMor (uncurry index . swap)
    (\(vect && idx), newValue => replaceAt idx newValue vect && ())

||| A board is a choice of 9 lenses that update a single row
choiceVect : DepParLens ? ? ?
choiceVect = updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}
 `extChoice` updateVect {n = 9} {a = Stone}

updateBoard : DepParLens ? (Const Board ) ?
updateBoard = reparam choiceVect
  (MkDepLens toProduct
           (\vect, diff => let (idx, value) = sumToSelector diff
                           in replaceAt idx value vect))

updateLeft : DepParLens
   (MkCont (Fin 9, Fin 9) (const Unit)) ? ?
updateLeft = (MkDepLens finToSum (\x => (const ())))
             `preCompose` updateBoard

boardLens : DepParLens ? ? (Const Stone)
boardLens = updateLeft `postCompose` MkDepLens (diagonal 9) (diaChoice {n = 9})

captures : DepParLens (MkCont (Fin 9 +/ Fin 9 +/ ()) (const ())) (Const Board) (Const Stone)
captures = MkDepLens (\x => MkPair x.head x.tail.head) (const id) `preCompose` boardLens

server : DepServer ? (Const Board) ?
server = let instance = constUnit in
             (Reparam (MkDepLens dup (\x => choice id (const x)))
                      (Lens captures +&&&+ Lens (viewParameter Board)))

partial
main : IO ()
main = runServer Go.server Normal emptyBoard

