module BIG

import Todo
import Calculator
import IOT
import Server
import Server.CLI
import Server.EDSL.Lens
import Data.SortedMap

%hide Prelude.(&&)

entireServer : DepServer ? ? ?
entireServer = "todo" / todoServer
         +&&&+ "calculator" / calculator
         +&&&+ "iot" / iotServer

main : IO ()
main = runServer entireServer Normal (empty && def)

