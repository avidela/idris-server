module Todo

import Server
import Server.Node
import Server.EDSL.Lens
import Server.EDSL.Path
import Data.String.Parser
import Data.Maybe
import Data.SortedMap
import Data.Product
import Data.Carrier

%hide Prelude.(/)
%hide Prelude.(*)
%hide Prelude.(+)

infixr 9 /

-- Update a value or add it if its missing
update : (def : v) -> k -> (v -> v) -> SortedMap k v -> SortedMap k v
update def k f m =
  case lookup k m of
       (Just v) => insert k (f v) m
       Nothing => insert k def m

-- a todo item is a title and a note about what is there to do
record Todo where
  constructor MkTodo
  title : String
  note : String

-- Todos are parsed as two strings separated with a comma
HasParser Todo where
  partialParse = MkTodo <$> str <* char ',' <*> str
      where str : Parser String
            str = pack <$> some alphaNum

Show Todo where
  show (MkTodo t n) = "Todo(title: \{t}, note: \{n})"

Display Todo where
  display = "Todo"

Default Todo where
  def = MkTodo "buy milk" "get the one on reduction"

-- Add a todo to the list of todos, if the user doesn't exist, add the todo as a singleton
addTodo : k -> v -> SortedMap k (List v) -> SortedMap k (List v)
addTodo key value = update (pure value) key (value ::)

-- The server state is a map of userIDs and lists of todos
public export
ServerState : Type
ServerState = SortedMap Nat (List Todo)

-- A lens that inspects the server state and add the todo given in argument using `addTodo`
addLens : DepParLens (MkCont Nat (const ()))
                     (Const ServerState)
                     (MkCont () (const Todo))
addLens = MkContMor (const ()) (\(state && id), value => addTodo id value state && ())

-- A lens that returns the list of todos from a user, return an empty list if the user isn't found
-- This could be simplified if we allowed vertical composition of lenses
getLens : DepParLens (MkCont Nat (const ())) (Const ServerState) (MkCont (List Todo) (const ()))
getLens = MkContMor (\(state && id) => fromMaybe [] (SortedMap.lookup id state)) (\(a && b), c => a && ())

-- The "get todo" endpoint is accessible through the url /all/id:Nat as a GET request
getTodos : DepServer (MkCont ? ?) (Const ServerState) (MkCont (List Todo) (const ()))
getTodos = MkPath ("all" / Nat / End) (Lens getLens)

-- The "add todo" endpoint is accessible through the url /add/id:Nat as a POST request
-- The body of the request needs to contain the todo item
postTodo : DepServer ? ? ?
postTodo = MkPath ("add" / Nat / End) (Lens addLens)

-- The server is the composition of the previous two endpoints
export
todoServer : DepServer ? ? ?
todoServer = getTodos &&& postTodo

main : IO ()
main = ignore $ runNodeServer todoServer Normal SortedMap.empty
