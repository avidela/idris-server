module Requests

import Data.String
import Data.List1
import Data.List
import Data.Maybe

public export
data Method
  = GET
  | POST
  | PUT
  | PATCH
  | UPDATE
  | DELETE

export
Show Method where
  show GET = "GET"
  show POST = "POST"
  show PUT = "PUT"
  show PATCH = "PATCH"
  show UPDATE = "UPDATE"
  show DELETE = "DELETE"

export
Eq Method where
  GET == GET = True
  POST == POST = True
  PUT == PUT = True
  PATCH == PATCH = True
  UPDATE == UPDATE = True
  DELETE == DELETE = True
  _ == _ = False

public export
data HTTPVersion : Nat -> Nat -> Type where
  V1991 : HTTPVersion Z 9
  V1996 : HTTPVersion 1 0
  V1997 : HTTPVersion 1 1
  V2015 : HTTPVersion 2 0
  V2020 : HTTPVersion 3 0

public export
record Request where
  constructor MkReq
  method : Method
  version : (min ** maj ** HTTPVersion min maj)
  headers : List (String, String)
  path : String
  body : String

export
Show Request where
  show (MkReq meth (n ** m ** ver) heads path body) = """
      \{show meth} \{path} \{showVersion}
      \{concat $ intersperse "\n" $ map (\(hd, con) => "\{hd}: \{con}") heads}
      \{body}
      """
      where
        showVersion : String
        showVersion = "HTTP/\{show n}.\{show m}"

export
Interpolation Request where
  interpolate (MkReq meth (n ** m ** ver) heads path body) = "\{show meth} \{path} | \{body}"

export
parseMethod : String -> Maybe Method
parseMethod "GET" = Just GET
parseMethod "POST" = Just POST
parseMethod "PATCH" = Just PATCH
parseMethod "PUT" = Just PUT
parseMethod "DELETE" = Just DELETE
parseMethod _ = Nothing

export
parseHTTPVersion : String -> Maybe (n ** m ** HTTPVersion n m)
parseHTTPVersion "HTTP/0.9" = Just (0 ** 9 ** V1991)
parseHTTPVersion "HTTP/1.0" = Just (_ ** _ ** V1996)
parseHTTPVersion "HTTP/1.1" = Just (_ ** _ ** V1997)
parseHTTPVersion "HTTP/2.0" = Just (_ ** _ ** V2015)
parseHTTPVersion "HTTP/3.0" = Just (_ ** _ ** V2020)
parseHTTPVersion _ = Nothing

||| Parse a request from a string
public export
parseRequest : String -> Maybe Request
parseRequest str =
  case words str of
       (method :: path :: xs) =>
                    let body = fromMaybe "" (head' xs) in
                    Just $
                       MkReq !(parseMethod method)
                       (2 ** 0 ** V2015)
                       [("User-Agent", "Idris/0.4")
                       ,("Host", "localhost")]
                       path
                       body
       _ => Nothing


