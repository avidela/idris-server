module Data.Action

import Data.Sum
import Data.Product
import Data.Container

%hide Prelude.(+)
%hide Prelude.(*)
%hide Prelude.(/)

public export
interface Action (0 c : Container) where
  iUpdate : (st : c.shp) -> c.pos st -> c.shp

||| override the old state with the new state
export
Action (Const st) where
  iUpdate old new = new

||| Keep the old state if the new state is empty
export
[constAction] Action (MkCont x (const Unit)) where
  iUpdate old new = old

export
[prodAction] Action p => Action q => Action (p * q) where
  iUpdate (o1 && o2) (<+ x) = iUpdate o1 x && o2
  iUpdate (o1 && o2) (+> x) = o1 && iUpdate o2 x

export
[tensorAction] Action p => Action q => Action (p `×` q) where
  iUpdate (o1 && o2) (x && y) = iUpdate o1 x && iUpdate o2 y

export
[constUnit] Action (MkCont x (const ())) where
  iUpdate x = const x

