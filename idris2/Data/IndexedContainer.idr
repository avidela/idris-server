
module Data.IndexedContainer

import Data.Sum
import Data.Product

public export
record ICont (i : Type) where
  constructor MkICont
  shape : Type
  positions : shape -> i -> Type

public export
choice : ICont i -> ICont i -> ICont i
choice x y = MkICont (x.shape + y.shape)
                     (Sum.choice x.positions y.positions)

public export
dirichlet : ICont i -> ICont i -> ICont i
dirichlet x y = MkICont (x.shape * y.shape) (\s, idx => x.positions s.π1 idx + y.positions s.π2 idx)

public export
parallel : ICont i -> ICont i -> ICont i
parallel x y = MkICont (x.shape * y.shape) (\s, idx => x.positions s.π1 idx  * y.positions s.π2 idx)

public export
record IContMor {i, j : Type} (c : ICont i) (d : ICont j) where
  constructor MkIContMor
  f : c.shape -> d.shape
  mapIdx : i -> j
  r : (s : c.shape) -> {x : i} -> d.positions (f s) (mapIdx x) -> c.positions s x

export
compose : {0 a : ICont i} -> {0 b : ICont j} -> {0 c : ICont k} ->
          IContMor a b -> IContMor b c -> IContMor a c
compose (MkIContMor f1 mapij b1) (MkIContMor f2 mapjk b2) =
  MkIContMor (f2 . f1)
             (mapjk . mapij)
             (\s, y => b1 s (b2 (f1 s) y))

