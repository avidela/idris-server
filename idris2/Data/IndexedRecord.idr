module Data.IndexedRecord

import public Records
import Interfaces
import public Data.String.ParserInterface

||| Value for a row given a key
public export
data RowVal : String -> Type -> Type where
  (:=:) : (s : String) -> (val : t) -> HasParser t => RowVal s t

public export
data IRecord : Record -> Type where
  Nil : IRecord []
  (::) :  HasParser t => RowVal s t -> Display t => Default t =>
          IRecord rs -> IRecord ((s :=: t) :: rs)
