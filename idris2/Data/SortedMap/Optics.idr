module Data.SortedMap.Optics

import Optics
import Data.SortedMap
import Data.Sum

||| Get the value at the given key
export
atIndex : SortedMap k v -> Prism v (k, v) k (SortedMap k v)
atIndex map = MkPrism (\key => maybe (<+ map) (+>) (lookup key map))
                      (\(key, val) => insert key val map)

export
atIndex' : CoCartesian p => SortedMap k v -> Optic p v (k, v) k (SortedMap k v)
atIndex' = prism2Pro . atIndex

