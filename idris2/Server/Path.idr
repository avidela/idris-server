module Server.Path

infixr 7 +/  -- path operators

||| A product type for path components
public export
record (+/) (a, b : Type) where
  constructor MkPathExt
  head : a
  tail : b

public export
Show a => Show b => Show (a +/ b) where
  show (MkPathExt a b) = "\{show a} / \{show b}"

