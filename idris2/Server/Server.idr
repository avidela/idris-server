module Server.Server

import public Interfaces

import Data.Product
import Data.String
import Data.Container
import public Data.IndexedRecord

%default total

public export
record ReqHandler (state : Type) where
  constructor MkReqHandler
  0 costate : state -> Type
  0 arguments : Type
  router : Parser arguments
  0 idx : Type
  0 requestBody : idx -> Type
  computeIndex : state * arguments -> idx
  bodyParser : {i : idx} -> Parser (requestBody i)
  0 response : arguments -> Type
  handle : (st : state * arguments) -> requestBody (computeIndex st) -> costate st.π1 * response st.π2
  update : (st : state) -> costate st -> state
  serialise : {arg : arguments} -> response arg -> String

public export
data ServerError : Type where
  UnexpectedPath : (expected, actual : String) -> ServerError
  ParseError : (message : String) -> ServerError
  UnhandledRequest : (message : String) -> (expected : String) -> ServerError
  Aggregate : List ServerError -> ServerError
  QueryLength : ServerError
  UnexpectedQueryItem : (expected, actual : String) -> ServerError

export
Show ServerError where
  show (UnexpectedPath expected actual) =
    "Expected path component " ++ show expected ++ ", got " ++ show actual ++ " instead."
  show (ParseError message) =
    "Parse error : \{message}."
  show (UnhandledRequest msg expected) =
    "Cannot handle \{msg} request, expected request: \{expected}"
  show (Aggregate xs) =
    "Tried multiple routes, all failed with errors: \n" ++ unlines (map (\x => " - " ++ (assert_total show) x) xs)
  show QueryLength =
    "Query items have the wrong length"
  show (UnexpectedQueryItem expected actual) =
    "Expected key '" ++ expected ++ "' got '" ++ actual ++ "' instead."

||| A server instance is a type that can be converted into a list of `ServerImpl` which describe how
||| heach path needs to be handled
public export
interface ServerInstance api c | api where
  toHandler : api -> ReqHandler c.shp
