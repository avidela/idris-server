module Server.EDSL.Path

import Data.Product
import Data.List
import Data.String.Singleton
import Data.String.ParserInterface
import Data.String.Parser
import Data.Carrier
import Data.Ops
import Server.Path
import Interfaces

%default total

||| A DSL to describe paths and path components
public export
data DSLPath : Type where
  DSLEnd : DSLPath
  DSLStr : String -> DSLPath -> DSLPath
  DSLTyp : (ty : Type) -> DSLPath -> DSLPath

public export
End : DSLPath
End = DSLEnd

||| Convert a DSLPath into a Type
public export
PathType : DSLPath -> Type
PathType  DSLEnd = Unit
PathType (DSLStr x y) = PathType y
PathType (DSLTyp x DSLEnd) = x
PathType (DSLTyp x t@(DSLStr y z)) = x * PathType t
PathType (DSLTyp x t@(DSLTyp y z)) = x * PathType t

||| An interface to overload the `/` operator for path construction
public export
interface PathBuilder ty where
  (/) : ty -> DSLPath -> DSLPath

public export
PathBuilder Type where
  a / b = DSLTyp a b

public export
PathBuilder String where
  (/) = DSLStr

public export
PathToParsable : DSLPath -> Type
PathToParsable DSLEnd = Unit
PathToParsable (DSLStr x y) = Str x +/ PathToParsable y
PathToParsable (DSLTyp x y) = Carrier x +/ PathToParsable y

public export
convert' : (from : DSLPath) -> PathType from -> PathToParsable from
convert' DSLEnd x = x
convert' (DSLStr y z) x = MkPathExt (MkS y) (convert' z x)
convert' (DSLTyp ty DSLEnd) x = MkPathExt (Carry x) ()
convert' (DSLTyp ty (DSLStr y z)) x = MkPathExt (Carry x.π1) (convert' (DSLStr y z) x.π2)
convert' (DSLTyp ty (DSLTyp y z)) x = MkPathExt (Carry x.π1) (convert' (DSLTyp y z) x.π2)

public export
convert : (from : DSLPath) -> PathToParsable from -> PathType from
convert  DSLEnd v = v
convert (DSLStr s t) (MkPathExt head tail) = convert t tail
convert (DSLTyp x DSLEnd) (MkPathExt v1 v2) = v1.value
convert (DSLTyp x t@(DSLStr _ _)) (MkPathExt v1 v2) = v1.value && convert t v2
convert (DSLTyp x t@(DSLTyp _ _)) (MkPathExt v1 v2) = v1.value && convert t v2

public export
splitParsers : HasParser (y * PathType w) -> (HasParser y, HasParser (PathType w))
splitParsers (MkParser pp) =
  (MkParser (map (.π1) pp), MkParser (map (.π2) pp))
