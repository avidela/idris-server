module Optics.Linear

record Lens (x, y, u, v : Type) where
  constructor MkLens
  1 fn : (1 _ : x) -> LPair u ((1 _ : v) -> y)

compose : (1 _ : Lens x y a b) -> (1 _ : Lens a b u v) -> Lens x y u v
compose (MkLens f) (MkLens g) = MkLens (\v => let v' # f' = f v
                                                  k1 # k2 = g v'
                                               in k1 # \y => f' (k2 y))
