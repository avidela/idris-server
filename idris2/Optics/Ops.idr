module Optics.Ops

infixr 4 |>    -- composition
infixl 2 <<<   -- pre-composition
infixr 2 >>>   -- post-composition
infixr 5 +&&&+ -- external choice
infixr 5 `extChoice`
infixr 5 &&&   -- clone choice
infixr 6 ***   -- parallel product

