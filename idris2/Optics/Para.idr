||| Para in terms of Lens.
module Optics.Para

import Optics

%hide Prelude.Basics.(&&)
%hide DPair.(.fst)
%hide DPair.(.snd)

K : b -> a -> b
K = const

public export
Para : (p, q, a, b, s, t : Type) -> Type
Para p q a b s t = Lens a b (s * p) (q * t)

public export
seq : Para p q a b s t -> Para p' q' s t x y -> Para (p * p') (q * q') a b x y
seq ((MkLens get1 set1)) ((MkLens get2 set2)) =
    MkLens (\(x && (p && p')) => get1 (get2 (x && p') && p))
           (\((x && (p && p')) && b) => let (q && t) = set1 ((get2 (x && p') && p) && b)
                                            (q' && y) = set2 ((x && p') && t)
                                         in (q && q') && y)

tensor : Para p q a b s t
          -> Para p' q' x y z w
          -> Para (p * p') (q * q') (a * x) (b * y) (s * z) (t * w)
tensor (MkLens get1 set1) (MkLens get2 set2) =
  MkLens (bimap get1 get2 . shuffle)
         (shuffle
         . bimap set1 set2
         . fork (bimap π1 π1)
                (bimap π2 π2)
         . mapFst shuffle)

reparam : Para p q a b s t
       -> Lens p q p' q'
       -> Para p' q' a b s t
reparam (MkLens get1 set1) (MkLens get2 set2) =
  MkLens (get1 . mapSnd get2)
         (fork (set2 . bimap (π2 . π1)
                             π1)
               (π2 . π2)
         . mapSnd (set1 . mapFst (mapSnd get2))
         . Product.dup)

monoTensor : Monoid q
          => Para p q a b s t
          -> Para p q x y z w
          -> Para p q (a * x) (b * y) (s * z) (t * w)
monoTensor l1 l2 =
  reparam (tensor l1 l2)
          (MkLens Product.dup (Product.uncurry (<+>) . π2))
