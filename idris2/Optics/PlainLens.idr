module Optics.PlainLens

import Data.Boundary
import Data.Product

%default total
%hide Prelude.(*)

public export
record Lens (l, r : Boundary) where
  constructor MkLens
  view : l.proj1 -> r.proj1
  update : l.proj1 -> r.proj2 -> l.proj2

export
compose : Lens l x -> Lens x r -> Lens l r
compose (MkLens v1 u1) (MkLens v2 u2) = MkLens
  (v2 . v1)
  (\st, val => u1 st (u2 (v1 st) val))

export
tensor : Lens l1 r1 -> Lens l2 r2 -> Lens (l1 `cartesian` l2) (r1 `cartesian` r2)
tensor {l1 = (MkB s t)} {r1 = (MkB a b)} {l2 = (MkB s' t')} {r2 = (MkB a' b')} (MkLens v1 u1) (MkLens v2 u2) = MkLens
  (bimap v1 v2)
  (\(s1, s2), (b1, b2) => (u1 s1 b1, u2 s2 b2))

choice : Lens l1 r1 -> Lens l2 r2 -> Lens (l1 `cocartesian` l2) (r1 `cocartesian` r2)
choice {l1 = (MkB s t)} {r1 = (MkB a b)} {l2 = (MkB s' t')} {r2 = (MkB a' b')} (MkLens v1 u1) (MkLens v2 u2) = MkLens
  (bimap v1 v2)
  (\st, val => ?iojoij) -- problem here, this is to demonstrate the limitation of plain lenses
                        -- and motivate the move toward dependent lenses

0
Para : (left, para, right : Boundary) -> Type
Para left para right = Lens (left `cartesian` para) right

export
pCompose : Para l p1 x -> Para x p2 r -> Para l (p1 `cartesian` p2) r
pCompose {l = (MkB s t)} {r = (MkB a b)} {x = (MkB m1 m2)}
         {p1 = (MkB p1 q1)} {p2 = (MkB p2 q2)}
         (MkLens v1 u1) (MkLens v2 u2) = MkLens
         (\arg => v2 (v1 (fst arg, fst (snd arg)), snd (snd arg)))
         (\arg, val => let res1 = u2 (v1 (fst arg, fst (snd arg)), snd (snd arg)) val;
                           res2 = u1 (fst arg, fst (snd arg)) (fst res1) in
                           (fst res2, (snd res2, snd res1)))

export
reparam : Para l p r -> Lens p' p -> Para l p' r
reparam {l = (MkB s t)} {r = (MkB a b)}
        {p = (MkB p q)} {p' = (MkB p' q')}
        (MkLens v1 u1) (MkLens v2 u2) = MkLens
        (\x => v1 (map v2 x))
        (\st, val => map (u2 (snd st)) (u1 (map v2 st) val))

-- Precomposition as a reparametrization
export
preCompose : Lens l x -> Para x p r -> Para l p r
preCompose {l = (MkB s t)} {x = (MkB x1 x2)} {p = (MkB p1 p2)} {r = (MkB a b)} y z =
  let withPara : Para (MkB s t) BUnit (MkB x1 x2)
               = MkLens (y.view . fst) (\st, arg => (y.update (fst st) arg, ()))
      composed : Para (MkB s t) (BUnit `cartesian` (MkB p1 p2)) (MkB a b)
               = pCompose {l = (MkB s t)} {x = MkB x1 x2} {r = MkB a b}
                          {p1 = (MkB Unit Unit)} {p2 = (MkB p1 p2)}
                          withPara z
      reparameterise : Lens (MkB p1 p2) (BUnit `cartesian` MkB p1 p2)
               = MkLens ((),) (const snd)
      in reparam {l = MkB s t}  {r = MkB a b}
                 {p' = MkB p1 p2} {p = BUnit `cartesian` MkB p1 p2}
                 composed reparameterise

paraTensor : Para l1 p1 r1 -> Para l2 p2 r2 -> Para (l1 `cartesian` l2) (p1 `cartesian` p2) (r1 `cartesian` r2)
paraTensor {l1 = MkB s1 t1} {l2 = MkB s2 t2}
           {p1 = MkB p1 q1} {p2 = MkB p2 q2}
           {r1 = MkB a1 b1} {r2 = MkB a2 b2}
           (MkLens v1 u1) (MkLens v2 u2) = MkLens
             (bimap v1 v2 . uncurry cartesian)
             (\st, arg => uncurry Pair.cartesian $ bimap (flip u1 (fst arg)) (flip u2 (snd arg)) $ uncurry Pair.cartesian st)


